# Personal Alacritty config

Color theme is based on [Nordfox](https://github.com/EdenEast/nightfox.nvim) (variant of Nightfox).   
Font is from [Nerd Fonts](https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/Noto/Mono).
